$.afui.autoLaunch = false;
//window.localStorage['usrJSON'] = '';

// Wait for device API libraries to load
//
document.addEventListener("deviceready", onDeviceReady, false);

// device APIs are available
//
function onDeviceReady() {
    //window.analytics.startTrackerWithId('UA-64415711-1');
    var conexion = checkConnection();

    if (conexion == 'No network connection')
    {
        $('#splashscreen').html('<div class="splashAlign">'+
                                    '<img src="assets/images/logo.png" class="splashLogo"><br />'+
                                    '<span style="color:#000;">No tienes conexión de internet.</span><br /><br />'+
                                    '<span style="color:#000;" data-action="reintentarLaunch">Probar de nuevo haciendo click aquí.</a>'+
                                '</div>');

        App._attachActionEvents();
    }
    else
    {
        App.checkJSON();
    }
}

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    //alert('Connection type: ' + states[networkState]);

    return states[networkState];
    //return 'No network connection';
    //return 'WiFi connection';
}


$(document).ready(function()
{
    //App.checkJSON();
});


var App = new function()
{
    this.intfc                      = 'http://tvdiscover.org/backoffice/assets/php/AppIntfc.php';
    this.post                       = 'http://tvdiscover.org/getJsons.php';
    this.rutaAbsImg                 = 'http://tvdiscover.org/backoffice/assets/cache/';    
    this.meses                      = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    this.dias                       = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
    this.region;
    this.data                       =
                                    {
                                        "user"              : '',
                                        'generos'           : '',
                                        'canales'           : '',
                                        'recomendaciones'   : '',
                                        'generosUsuario'    : new Array(),
                                        'canalesUsuario'    : new Array()
                                    };
    
    //CAMARA
    this.pictureSource;
    this.destinationType;
    this.rutaImagen         = '';
    
    /**
    * Inicializa la Aplicación
    */
    this.init = function()
    {
        App.data.generos = JSON.parse(window.localStorage['genJSON']);                
        App.data.canales = JSON.parse(window.localStorage['canJSON']);                
        
        //OAuth.initialize('V1zLogqoqoX8iMtZrEQx12KhsJY');
        
        $.afui.launch();
        $('header').hide();
        
        $('.owl-carousel').owlCarousel(
        {
            loop:true,
            items: 1,
            nav:false
        });
        
        
        setTimeout(function()
        {
            App.goToPantallaInicial();
        },10);
        
        
        
        App._attachActionEvents();

    };    

    
    this.goBack = function()
    {
        var id = '';
        
        $.afui.goBack();                
        
        $('#cuerpoApp .panel').each(function()
        {
            var clases = $(this).attr('class');
            
            if (clases.indexOf('active')!= -1)
            {
                id = $(this).attr('id')
            }
        });
        
        App.showFooter(id);
        
    }
    
    this.showMenu = function()
    {
        $.afui.drawer.show("#left","left","reveal");
    }
    
    this.resetFooterImg = function()
    {
        $('footer .row').hide();
        $('footer .row.footer-recomendaciones').show();
        
        $('.footer-recomendaciones .rec').html('<img src="assets/images/recomendaciones/ft1.png">');
        $('.footer-recomendaciones .now').html('<img src="assets/images/recomendaciones/ft2.png">');
        $('.footer-recomendaciones .prf').html('<img src="assets/images/recomendaciones/ft5.png">');
        
    }
    
    this.showFooter = function(id)
    {
        //console.log('footer '+id);
        
        $('footer .row').hide();
        $('footer .row.footer-'+id).show();
        
        $('.hdUpgrade').hide();
        $('.generoReinicio').hide();
        $('.canalesReinicio').hide();
        $('.hdEdit').hide();
        
        switch (id)
        {
            case 'profile':
                $('header').show();
                $('.hdTit').addClass('blanco');
                $('.hdTit').html('Perfil');                
                $('.hdFlecha').hide();
                $('.hdNext').hide();
                
                $('.header').show();
                
                $('.hdUpgrade').show();
                
                App.resetFooterImg();
                $('.footer-recomendaciones .prf').html('<img src="assets/images/recomendaciones/ft5S.png">');
            break;
            
            case 'now':
                $('header').show();
                $('.hdTit').addClass('blanco');
                $('.hdTit').html('Ahora');                
                $('.hdFlecha').hide();
                $('.hdNext').hide();
                
                $('.header').show();
                
                App.resetFooterImg();
                $('.footer-recomendaciones .now').html('<img src="assets/images/recomendaciones/ft2S.png">');
            break;
            
            case 'home':
                $('header').hide();
            break;
        
            case 'registro':
                $('header').show();
                $('.hdTit').html('Registro');
                $('.hdFlecha').show();
                $('.hdNext').hide();
                
                $('.hdTit').removeClass('blanco');
                $('.header').hide();
            break;
        
            case 'login':
                $('header').show();
                $('.hdTit').html('Iniciar sesión');
                $('.hdFlecha').show();
                $('.hdFlecha.blanca').hide();
                $('.hdNext').hide();
                
                $('.hdTit').removeClass('blanco');
                $('.header').hide();
            break;
        
            case 'generos':
                $('header').show();
                $('.hdTit').html('');
                $('.hdFlecha').hide();
                
                $('.hdNext .btnAzul').attr('data-action','showLocalidad')
                $('.hdNext').show();
                
                App._attachActionEvents();
            break;
        
            case 'localidad':
                $('header').show();
                $('.hdTit').html('');
                $('.hdFlecha').hide();
                
                $('.hdNext .btnAzul').attr('data-action','validarLocalidad')
                $('.hdNext').show();
                
                App._attachActionEvents();
            break;
        
            case 'canales':
                $('header').show();
                $('.hdTit').html('');
                $('.hdFlecha').hide();
                
                $('.hdNext .btnAzul').attr('data-action','validarCanales')
                $('.hdNext').show();
                
                App._attachActionEvents();
            break;  
        
            case 'recomendaciones':
                $('header').show();
                $('.hdTit').addClass('blanco');
                $('.hdTit').html('Discover');                
                $('.hdFlecha').hide();
                $('.hdNext').hide();
                
                $('.header').show();
                
                App.resetFooterImg();
                $('.footer-recomendaciones .rec').html('<img src="assets/images/recomendaciones/ft1S.png">');
            break;
        
            case 'programaDetalle':
                $('header').show();
                $('.hdTit').removeClass('blanco');
                $('.hdTit').html('Detalle');                
                $('.hdFlecha').show();
                $('.hdFlecha.blanca').hide();
                $('.hdNext').hide();
                
                $('.header').hide();
            break;
        
            case 'programaComunidad':
                $('header').show();
                $('.hdTit').removeClass('blanco');
                $('.hdTit').html('Comunidad');                
                $('.hdFlecha').show();
                $('.hdNext').hide();
                
                $('.header').hide();
            break;
        
            case 'premium':
                $('header').show();
                
                $('.hdTit').html('');
                $('.hdFlecha').show();
                $('.hdFlecha.blanca').hide();
                $('.hdNext').hide();
                $('.hdTit').removeClass('blanco');
                $('.header').hide();
            break;
        
            
        }
    }
    
    this.changePanel = function(id)
    {
        App.showFooter(id.substring(1));
        $.afui.loadContent(id,false,false,"right");
    }
    
    this.popupMessage = function(title,message)
    {
        $.afui.popup( {
            title           : title,
            message         : message,
            cancelOnly      : true,
            cancelText      : 'Aceptar'
        });
    }
    
    this.goToPantallaInicial = function()
    {
        if (App.data.user != '')
        {
            //console.log(App.data.user);
            /*
            console.log('generos: '+App.data.user.generos.length );
            console.log('localidad: '+App.data.user.tv_usuarios_region );
            console.log('canales: '+App.data.user.canales.length);
            */
            
            if (App.data.user.generos.length == 0)
            {
                App.showGeneros();
            }
            else
            if (App.data.user.tv_usuarios_region == null || App.data.user.tv_usuarios_region == '')
            {
                App.changePanel('#localidad');
            }
            else
                App.showRecomendaciones();
            /*if (App.data.user.canales.length>0)
            {
                App.showRecomendaciones();
            }
            else
            if (App.data.user.canales.length==0)
            {
                App.showCanales();
            }
            else
            {
                App.changePanel('#home');    
            }*/
            
            /*
            if (App.data.user.canales.length>0)
            {
                App.showRecomendaciones();
            }
            else if (App.data.user.tv_usuarios_region!='')
            {
                App.showCanales();
            }
            else if (App.data.user.generos.length>0)
            {
                App.changePanel('#localidad');
            }
            else
            {
                App.showGeneros();    
            }*/
        }
        else
        {
            App.changePanel('#home');    
        }
    }
    
    /*****************************************************
     * REGISTRO
     *****************************************************/
    this.showRegistro = function()
    {
        App.changePanel('#registro');
    }
    
    //Valida que el repetir password sea igual
    this.repeatPassword = function(element)
    {
        element = $(element);
        if(element.val() == $('#contrasena').val() && element.val()!='')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    this.validarUsuario = function()
    {
        $.validity.setup({ outputMode: "red"});
        $.validity.start();

        $("#registro .req").require();
        $("#registro .req-mail").require().match('email');
        $("#registro .req-repeat").assert( App.repeatPassword, 'Las contraseñas no coinciden.');

        var result = $.validity.end();
        console.log(result.errors);
        if (result.errors==0)
        {
            var data =
            {
                'nombre'    : $('#nombre').val(),
                'password'  : $('#contrasena').val(),
                'mail'      : $('#mail').val(),
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'insertUser',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log('done...');
                console.log(data);
                
                App.showGeneros();
                
                window.localStorage['usrJSON'] = data;
            
                data = JSON.parse(data);
                App.data.user = data;
            });
        }
    }
    
    this.loginFB = function()
    {
        console.log('loginFB');
        OAuth.popup('facebook',
        {
            cache: true
        })
        .done(function(result)
        {
            console.log(result);
            //use result.access_token in your API request
            //or use result.get|post|put|del|patch|me methods (see below)
            result.get('/me')
            .done(function (data)
            {
                console.log('loginFB');
                console.log(data);
                
                var posting = $.post(App.intfc,
                {
                    action  : 'insertJsonUser',
                    data    : JSON.stringify(data),
                    provider: 'facebook'
                });

                posting.done(function(data)
                {
                    App.callbackSocialLogin(data);
                });


            })
            .fail(function (err)
            {
                App.popupMessage('Login Facebook','No se pudieron obtener los datos de Facebook.');
            });
        })
        .fail(function (err)
        {
            //handle error with err
            console.log('login/fail '+err);
            App.popupMessage('Login Facebook','El usuario cancelo el login.');
        });

    }

    this.loginTW = function()
    {

        OAuth.popup('twitter',
        {
            cache: true
        })
        .done(function(result)
        {
            result.me()
            .done(function(data)
            {
                //this will display "John Doe" in the console
                //$('#login').html(response.name);


                var posting = $.post(App.intfc,
                {
                    action  : 'insertJsonUser',
                    data    : JSON.stringify(data),
                    provider: 'twitter'
                });

                posting.done(function(data)
                {
                    App.callbackSocialLogin(data);
                });


            })
            .fail(function (err)
            {
                App.popupMessage('Login Twitter','No se pudieron obtener los datos de Twitter.');
            });
        })
        .fail(function (err)
        {
            //handle error with err
            console.log('login/fail '+err);
            App.popupMessage('Login Twitter','El usuario cancelo el login.');
        });

    }
    
    this.callbackSocialLogin = function(data)
    {
        console.log('callbackSocialLogincallbackSocialLogincallbackSocialLogincallbackSocialLogincallbackSocialLogin');
        console.log(data);
        
        window.localStorage['usrJSON'] = data;
        App.data.user  = JSON.parse(window.localStorage['usrJSON']);

        App.goToPantallaInicial();
    }
    
    /*****************************************************
     * LOGIN
     *****************************************************/    
    this.logout = function()
    {
        window.localStorage['usrJSON'] = '';
        App.changePanel('#home'); 
    }
    
    this.showLogin = function()
    {
        App.changePanel('#login');
    }
    
    this.iniciarSesion = function()
    {
        $.validity.setup({ outputMode: "red"});
        $.validity.start();

        $("#login .req-mail").require().match('email');

        var result = $.validity.end();
        
        if (result.errors==0)
        {
            var data =
            {
                'usr'   : $('#loginUsr').val(),
                'pwd'   : $('#loginPwd').val(),
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'loginUser',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log(data);
                
                if (data == 'false')
                {
                    App.popupMessage('TV Discover','El usuario no existe o la contraseña no coincide.');
                }
                else
                {
                    window.localStorage['usrJSON'] = data;
            
                    data = JSON.parse(data);
                    App.data.user = data;
                    
                    App.goToPantallaInicial();
                }
            });
        }
    }
    
    this.recuperarContrasena = function()
    {
        var mail = $('#loginUsr').val();
        if (mail=='')
        {
            App.popupMessage('Recuperar contraseña','Favor de escribir tu mail de registro.');
        }
        else
        {
            App.popupMessage('Recuperar contraseña','Se envío un correo a : '+mail);

            var posting = $.post(App.intfc,
            {
                action  : 'sendMailContrasena',
                data    : mail
            });

            posting.done(function(data)
            {

                console.log(data);

                /*
                App.showHeader('home');
                App.changePanel('#home');*/
            });
        }
    }
    
    this.showGeneros= function()
    {
        App.data.generosUsuario = new Array();
        $('#container').hide();
        
        var html = '';
        
        $.each(App.data.generos,function(i,val)
        {
           
                var color = 'azul';
            
                var tam = 2;
                
                //html += '<div class="btnGenero '+color+' >'+val.contentChannel+'</div>';
                html += '<div class="bubble '+color+'" data-action="selectGenero" data-id="'+val.tv_generos_id+'"><span>'+val.tv_generos_nombre+'</span></div>';//code
        });
        
        
        
        $('.bubbles').html(html);
        $('#container').fadeIn();
       
        
        setTimeout(function()
        {
            App.setLayoutBubbles();
            
            App._attachActionEvents();
            
        },600)
    
        //$('.grid').css('height','auto');
        //$('.generos').jGravity();
        App.changePanel('#generos');
    }
    
    this.setLayoutBubbles = function()
    {
        var bubbles = $('.bubble');
        
        var total = Math.floor(bubbles.length/4) * 2;
        var posINIX = ((total*100)/2)-50;
        
        //$('.bubbles').width('1500px')
        $('.bubbles').width(((total)*100)+'px')        
        $('#container').animate( { scrollLeft: '+='+(((total*100)/2)-150) }, 1000);
        
        
        $(bubbles[0]).css(
        {
            'position'      :'absolute',
            'top'           :'150px',
            'left'          :posINIX+'px',
            'z-index'       :'5',
            'margin-top'    :'-50px',
            'margin-left'   :'-50px',
        });
        
        //console.log('width inicial: '+$(bubbles[0]).width())
        //console.log('position inicial: '+posINIX)
        //console.log('offset inicial: '+$(bubbles[0]).offset().left)
        
        //var posINIX = 750;//$(bubbles[0]).position().left;
        
        var posI = 0;
        var posIT = 0;
        var posIR = 0;
        var posIB = 0;
        
        var posD = 0;
        var posDT = 0;
        var posDR = 0;
        var posDB = 0;
        
        $.each(bubbles,function(i,el)
        {
            if (i>0)
            {
                el = $(el);
                //console.log(el);
                
                if (i%2==0)
                {
                    //console.log('derecha');
                    var top = 0;
                    switch(posI)
                    {
                        case 0://arriba
                            
                            var posX = (posINIX+(50+(posIT*100)));
                            
                            top     = '22%';
                            left    = posX+'px';
                            //console.log('izquierda: '+posX);
                            posIT++;
                        break;
                    
                        case 1://derecha
                            var posX = (posINIX + (100*(posIR+1)));   
                            top     = '50%';
                            left    = posX+'px';
                            
                            posIR++;
                        break;
                    
                        case 2://abajo
                            var posX = (posINIX+(50+(posIB*100)));
                            
                            top     = '78%';
                            left    = posX+'px';
                            
                            posIB++;
                        break;
                    }
                    
                    el.css(
                    {
                        'position'      :'absolute',
                        'top'           :top,
                        'left'          :left,
                        'margin-top'    :'-50px',
                        'margin-left'   :'-50px',
                    });
                    
                    posI++;
                    if (posI==3)
                    {
                        posI = 0;
                    }
                }
                else
                {
                    //console.log('izquierda');
                    var top = 0;
                    switch(posD)
                    {
                        case 0://arriba
                            
                            var posX = (posINIX - (100*(posDR+1)))+50;                           
                            top     = '22%';
                            left    = posX+'px';
                            
                            //console.log('derecha: '+posX);
                            
                            posDT++;
                        break;
                    
                        case 1://izquierda
                            
                            var posX = (posINIX - (100*(posDR+1)));                            
                            
                            top     = '50%';
                            left    = posX+'px';
                            
                            posDR++;
                        break;
                    
                        case 2://abajo
                            var posX = (posINIX - (100*(posDB+1)))+50;
                            
                            top     = '78%';
                            left    = posX+'px';
                            
                            posDB++;
                        break;
                    }
                    
                    //console.log(left);
                    
                    el.css(
                    {
                        'position'      :'absolute',
                        'top'           :top,
                        'left'          :left,
                        'margin-top'    :'-50px',
                        'margin-left'   :'-50px',
                    });
                    
                    posD++;
                    if (posD==3)
                    {
                        posD = 0;
                    }
                    
                    
                    
                }
            }
            
        });
    }
    
    this.showMasGeneros = function()
    {
        
        var boxes = [],
            count = Math.random()*15;
            if (count < 5) count = 5;
      
        for (var i=0; i < count; i++ )
        {
            var color = App.randomIntFromInterval(1,2);
            if (color == 1)
            {
                color = 'verde';
            }
            else
            {
                color = 'azul';
            }
            
            var tam = App.randomIntFromInterval(1,3);
            
          var box = document.createElement('div');
          box.className = 'box ' + color+' size'+ tam+''+tam;
          box.innerHTML = 'prueba';
          // add box DOM node to array of new elements
          boxes.push( box );
        }
        
        
        $('#container').prepend(boxes).nested('prepend',boxes);
        
        $('#container .box').each(function()
        {
            var wd = $(this).data('width');
            //console.log($(this).data('width'));
            
            $(this).css('line-height',wd+'px');
        });
    }
    
    //Esconde el género seleccionado
    this.selectGenero = function(event)
    {
        var el      = $(event.delegateTarget);
        var id      = el.data('id');
        
        $('.generoReinicio').fadeIn();
        
        if (el.hasClass('azul'))
        {
            if (App.data.generosUsuario.length<5)
            {
                el.removeClass('azul');
                el.addClass('verde');
                //$('#container').nested();
                
                App.data.generosUsuario.push(id);    
            }
            else
            {
                App.popupMessage('TV Discover','La versión gratuita sólo permite 5 géneros.');
            }
        }
        else
        {
            el.removeClass('verde');
            el.addClass('azul');
            
            var indiceBorrar = -1;
            
            $.each(App.data.generosUsuario,function(i,val)
            {
                if (val==id)
                {
                    indiceBorrar = i;
                    return -1;
                }
            });
            
            if (indiceBorrar > -1)
            {
                App.data.generosUsuario.splice(indiceBorrar, 1);
            }
        }
        
        $('.contadorGeneros').html(App.data.generosUsuario.length+'/5');
        
        
    }
    
    this.showLocalidad = function()
    {
        if (App.data.generosUsuario.length>0)
        {
            var data =
            {
                'idUser' : App.data.user.tv_usuarios_id,
                'generos': App.data.generosUsuario
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'setUserGeneros',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log('done...');
                console.log(data);
                
                window.localStorage['usrJSON'] = data;
            
                data = JSON.parse(data);
                App.data.user = data;
            });
            
            App.changePanel('#localidad');
        }
        else
        {
            App.popupMessage('TV Discover','Favor de seleccionar al menos un género.');
        }
        
    }
    
    this.setLocalidad = function(event)
    {
        var el      = $(event.delegateTarget);
        var param   = el.data('param');
        
        $('.region .paloma').hide();
        el.find('.paloma').show();
        
        App.region = param;
    }
    
    this.validarLocalidad = function()
    {
        if (App.data.user.tv_usuarios_region=='')
        {
            App.popupMessage('TV Discover','Favor de seleccionar tu localidad.');
        }
        else
        {
            var region = App.region;
            
            if (region == '')
            {
                App.popupMessage('TV Discover','Favor de seleccionar tu localidad.');
            }
            else
            {
                var data =
                {
                    'idUser' : App.data.user.tv_usuarios_id,
                    'region' : region
                };
                
                var posting = $.post(App.intfc,
                {
                    action  : 'setUserLocalidad',
                    data    : data
                });
                
                posting.done(function(data)
                {
                    console.log('done...');
                    console.log(data);
                    
                    window.localStorage['usrJSON'] = data;
                
                    data = JSON.parse(data);
                    App.data.user = data;
                });
                
                App.showCanales();    
                
            }
            
        }
    }
    
    this.showCanales = function()
    {
        App.data.canalesUsuario = new Array();
        
        App.generateCanales();
        App.changePanel('#canales');
        
    }
    
    this.generateCanales = function()
    {
        var html = '';
        
        $.each(App.data.canales.publicos,function(i,val)
        {
            if (App.data.user.tv_usuarios_region == val.tv_canales_region)
            {
                var img = App.rutaAbsImg+val.foto.core_cloudfiles_filename+val.foto.core_cloudfiles_extension;
            
                //html += '<div class="canal" data-action="selectCanal" data-id="'+val.tv_canales_id+'">'+
                html += '<div class="canal">'+
                            '<div class="foto"><img src="'+img+'"></div>'+
                            '<div class="nombre">'+val.tv_canales_nombre+'</div>'+
                        '</div>';
            }
            
        });
        
        $.each(App.data.canales.privados,function(i,val)
        {
            var img = App.rutaAbsImg+val.foto.core_cloudfiles_filename+val.foto.core_cloudfiles_extension;
            
            //html += '<div class="canal" data-action="selectCanal" data-id="'+val.tv_canales_id+'">'+
            html += '<div class="canal">'+
                        '<div class="foto"><img src="'+img+'"></div>'+
                        '<div class="nombre">'+val.tv_canales_nombre+'</div>'+
                    '</div>';
        });
        
        $('.canales').html(html);
    }
    
    
    
    this.validarCanales = function()
    {
        App.changePanel('#recomendaciones');
        /*console.log(App.data.canalesUsuario);
        
        if (App.data.canalesUsuario.length>0)
        {
            var data =
            {
                'idUser' : App.data.user.tv_usuarios_id,
                'canales': App.data.canalesUsuario
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'setUserCanales',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log('done...');
                console.log(data);
                
                window.localStorage['usrJSON'] = data;
            
                data = JSON.parse(data);
                App.data.user = data;
            });
            
            App.changePanel('#recomendaciones');
        }
        else
        {
            App.popupMessage('TV Discover','Favor de seleccionar al menos un canal.');
        }*/
    }
    
    
    /*****************************************************
     * RECOMENDACIONES Y PROGRAMAS
     *****************************************************/    
    this.showRecomendaciones = function()
    {
        App.changePanel('#recomendaciones');
        
        $('.btnRec').removeClass('selected');
        $('.btnRec:first').addClass('selected');
        
        var data = {
            'generos' : App.data.user.generos,
            'canales' : App.data.user.canales,
            'fecha'   : moment().format('YYYY-MM-DD'),
            'user'    : App.data.user
        };
        
        var posting = $.post(App.intfc,
        {
            action	: 'getRecomendaciones',
            data    : data
        });
        
        posting.done(function(data)
        {
            if (data!='[]')
            {
                data = JSON.parse(data);            
                App.data.recomendaciones = data;
                
                App.setRecomendaciones();            
            }
            else
            {
                $('.recHorario').html('');
                $('.recomendaciones').html('<center><br><br>No tienes recomendaciones, intenta cambiar de día.</center>');
            }
            
        });
        
    }
    
    this.changeRecomendaciones = function(event)
    {
        var el      = $(event.delegateTarget);
        var dia     = el.data('param');
        var fecha;
        
        $('.btnRec').removeClass('selected');
        el.addClass('selected');
        
        switch(dia)
        {
            case 'now':
                fecha = moment().format('YYYY-MM-DD');
            break;
        
            case 'tomorrow':
                fecha = moment().add(1,'d').format('YYYY-MM-DD');
            break;
        
            case 'week':
                fecha = moment().add(2,'d').format('YYYY-MM-DD');
            break;
        }
        
        console.log('recomendaciones... '+dia+ ' '+fecha);
        
        //App.changePanel('#recomendaciones');
        
        App.popupMessage('TV Discover','cargando recomendaciones...');
        //$.afui.popup('Cargando recomendaciones...');
        
        var data = {
            'generos' : App.data.user.generos,
            'canales' : App.data.user.canales,
            'fecha'   : fecha,
            'user'    : App.data.user
        };
        
        var posting = $.post(App.intfc,
        {
            action	: 'getRecomendaciones',
            data    : data
        });
        
        posting.done(function(data)
        {
            $('.afPopup div:first').html('listo');
            
            if (data!='[]')
            {
                data = JSON.parse(data);            
                App.data.recomendaciones = data;
                
                App.setRecomendaciones();            
            }
            else
            {
                $('.recHorario').html('');
                $('.recomendaciones').html('<center><br><br>No tienes recomendaciones, intenta cambiar de día.</center>');
            }
            
        });
        
        
    }
    
    this.setRecomendaciones = function()
    {
        var html = '';
        var limiteHorario = {
            'minimo' : moment().add(2,'d'),
            'maximo' : moment().subtract(2,'d')            
        }
        
        //console.log('recomendaciones');
        //console.log(App.data.recomendaciones);
        
        
        $.each(App.data.recomendaciones,function(i,el)
        {
            if (el.tv_programacion_horario != undefined)
            {
                var horario     = el.tv_programacion_horario;
                var hasHorario  = horario.indexOf(" ");
                
                if (hasHorario != -1)
                {
                    horario     = horario.split(' ');
                
                    var fecha   = horario[0].split('-');
                    var hora    = horario[1].split(':');
                    
                    horario     = horario[1];            
                    horario     = horario.substring(0,5);
                    
                    var horarioMoment = moment().set({
                        'year'  : fecha[0],
                        'month' : fecha[1],
                        'date'  : fecha[2],
                        'hour'  : hora[0],
                        'minute': hora[1],
                        'second': hora[2],
                    });
                    
                    if (horarioMoment < limiteHorario.minimo  )
                    {
                        limiteHorario.minimo = horarioMoment;
                    }
                    
                    if ( horarioMoment > limiteHorario.maximo )
                    {
                        limiteHorario.maximo = horarioMoment;
                    }
                    
                    var imagen  = App.rutaAbsImg+el.foto.core_cloudfiles_filename+el.foto.core_cloudfiles_extension;
                    var logo    = App.rutaAbsImg+el.canal.foto.core_cloudfiles_filename+el.canal.foto.core_cloudfiles_extension;
                    
                    html += '<div class="recPrograma" data-action="showProgramaDetalle" data-id="'+i+'">'+
                                '<div class="genero">Género del programa</div>'+
                                '<div class="nombre">'+el.tv_programas_nombre+'</div>'+
                                '<div class="canal"><img src="'+logo+'"></div>'+
                                '<div class="horario">'+horario+'</div>'+
                                '<div class="fondo" style="background:url('+imagen+') no-repeat top center; background-size:cover;"></div>'+
                            '</div>'
                }
                
            }
            
            
            
        });
        
        var minimo = moment(App.data.recomendaciones[0].tv_programacion_horario);
        var maximo = moment(App.data.recomendaciones[(App.data.recomendaciones.length - 1)].tv_programacion_horario);
        
        
        $('.recomendaciones').html(html);
        $('.recHorario').html('de '+minimo.format('H:mm')+' a '+maximo.format('H:mm'));
        
        App._attachActionEvents();
        
    }
    
    this.showProgramaDetalle = function(event)
    {
        var el      = $(event.delegateTarget);
        var id      = el.data('id');
        
        var programa = App.data.recomendaciones[id];
        
        console.log(programa);
        
        var imagen   = App.rutaAbsImg+programa.foto.core_cloudfiles_filename+programa.foto.core_cloudfiles_extension;
        
        var horario = programa.tv_programacion_horario;
        horario     = horario.split(' ');
        var fecha   = horario[0].split('-');
        var hora    = horario[1].split(':');
        
        horario     = horario[1];            
        horario     = horario.substring(0,5);
        
        var horarioMoment = moment().set({
            'year'  : fecha[0],
            'month' : fecha[1],
            'date'  : fecha[2],
            'hour'  : hora[0],
            'minute': hora[1],
            'second': hora[2],
        });
        
        moment.locale('es');
        
        var rating = Number(programa.rating).toFixed(2);
        
        $('#programaDetalle .rating').html(rating+'<span>Rating de la comunidad</span>');
        $('#programaDetalle .progEstrellas').attr('data-programa',programa.tv_programas_id)
        $('#programaDetalle .horario').html(horarioMoment.locale('es').format('ddd')+' - '+horarioMoment.format('H:mm'));
        $('#programaDetalle .nombre').html(programa.tv_programas_nombre);
        $('#programaDetalle .fondo').css({
            'background'        : 'url('+imagen+') no-repeat top center',
            'background-size'   : 'cover'
        });
        
        $('#programaDetalle .btnVerMas').html('Ver más');
        $('#programaDetalle .progDesc').html(programa.tv_programas_descripcion);
        
        $('#programaDetalle .estrella').html('<img src="assets/images/programa/estrellaVacia.png">');
        $('#programaDetalle .estrella').each(function()
        {
            if ($(this).data('calif')<=programa.calificacion)
            {
                $(this).html('<img src="assets/images/programa/estrella.png">');
            }
        });
        
        var desc = programa.tv_programas_descripcion.length;
        
        
        if (desc > 150)
        {
            $('#programaDetalle .btnVerMas').show();
        }
        else
        {
            $('#programaDetalle .btnVerMas').hide();
        }
        
        $('.progOpciones .texto').attr('data-nombre', programa.tv_programas_nombre );
        $('.progOpciones .texto').attr('data-fecha' , programa.tv_programacion_horario);
        $('.progOpciones .texto').attr('data-desc'  , programa.tv_programas_descripcion);
        
        App.changePanel('#programaDetalle');
        
    }
    
    this.showVerMasDecripcion = function()
    {
        var hg = $('#programaDetalle .progDesc').height();
        
        if (hg==105)
        {
            $('#programaDetalle .progDesc').css('height','auto');
            $('#programaDetalle .btnVerMas').html('Ver menos');
        }
        else
        {
            $('#programaDetalle .progDesc').css('height','105px');
            $('#programaDetalle .btnVerMas').html('Ver más');
        }
    }
    
    this.setCalificacion = function(event)
    {
        var el          = $(event.delegateTarget);
        var calif       = el.data('calif');
        
        $('#programaDetalle .estrella').html('<img src="assets/images/programa/estrellaVacia.png">');
        $('#programaDetalle .estrella').each(function()
        {
            if ($(this).data('calif')<=calif)
            {
                $(this).html('<img src="assets/images/programa/estrella.png">');
            }
        });
    
        var data = {
            'user'          : App.data.user.tv_usuarios_id,
            'idPrograma'    : $('#programaDetalle .progEstrellas').data('programa'),
            'calif'         : calif
        }
        
        var posting = $.post(App.intfc,
        {
            action  : 'setCalificacion',
            data    : data
        });
        
        posting.done(function(data)
        {
            var dataPost = {
                'generos' : App.data.user.generos,
                'canales' : App.data.user.canales,
                'fecha'   : moment().format('YYYY-MM-DD'),
                'user'    : App.data.user
            };
            
            var posting = $.post(App.intfc,
            {
                action	: 'getRecomendaciones',
                data    : dataPost
            });
            
            posting.done(function(data)
            {
                $('.btnRec').removeClass('selected');
                $('.btnRec:first').addClass('selected');
                
                if (data!='[]')
                {
                    //console.log(data);
                    data = JSON.parse(data);            
                    App.data.recomendaciones = data;
                    
                    var idPrograma = $('#programaDetalle .progEstrellas').data('programa');
                    $.each(App.data.recomendaciones,function(i,el)
                    {
                        
                        if (el.programacion.tv_programas_id == idPrograma)
                        {
                            
                            var rating = Number(el.programacion.rating).toFixed(2);
                            $('#programaDetalle .rating').html(rating+'<span>Rating de la comunidad</span>');
                            
                            return false;
                        }
                    })
                    
                    App.setRecomendaciones();
                }
                else
                {
                    $('.recHorario').html('');
                    $('.recomendaciones').html('<center><br><br>No tienes recomendaciones, intenta cambiar de día.</center>');
                }
                
                
                
                
                
            });
            
            
            App.popupMessage('TV Discover','Calificación guardada.');
            
        });
        
    }
    
    this.crearRecordatorio = function(event)
    {
        var el          = $(event.delegateTarget);
        var nombre      = el.data('nombre');
        var fecha       = el.data('fecha');
        var desc        = el.data('desc');
        
        console.log(nombre);
        console.log(fecha);
        console.log(desc);
        
        var horario = fecha;
        horario     = horario.split(' ');
        var fecha   = horario[0].split('-');
        var hora    = horario[1].split(':');
        
        var startDate = new Date(fecha[0],fecha[1],fecha[2],hora[0],hora[1],hora[2],0,0);
        var endDate   = new Date(fecha[0],fecha[1],fecha[2],hora[0],hora[1],hora[2],0,0);
        endDate.setHours(endDate.getHours()+1);
        
        console.log(startDate);
        console.log(endDate);
        
        var title1          = 'TV Discover';
        var loc             = 'Recordatorio';
        
        var calendarName    = "TVDiscover";
        var notes           = desc;
        
        App.createCalendarEvent(title1, loc, notes, startDate, endDate);
        window.plugins.calendar.openCalendar(startDate);
    }
    
    
    this.showRecomendacionesNow = function()
    {
        App.changePanel('#now');
    }
    
    /*****************************************************
     * CALENDARIO
     *****************************************************/    
    this.createCalendarEvent= function(title, loc, notes, startDate, endDate)
    {
        window.plugins.calendar.createEvent(title, loc, notes, startDate, endDate, App.onSuccess, App.onError);    
    }
    
    /*****************************************************
     * COMUNIDAD
     *****************************************************/    
    this.showComunidad = function(event)
    {
        var el      = $(event.delegateTarget);
        var id      = el.data('id');
        
        App.changePanel('#programaComunidad');
    }
    
    /*****************************************************
     * PROFILE
     *****************************************************/    
    this.showProfile = function()
    {
        $('.profDatos .nombre').html(App.data.user.tv_usuarios_nombre);
        
        console.log(App.data.user.tv_usuarios_id_foto);
        if (App.data.user.tv_usuarios_id_foto != '' && App.data.user.tv_usuarios_id_foto != null && App.data.user.tv_usuarios_id_foto != 0)
        {
            var img = App.rutaAbsImg+App.data.user.foto.core_cloudfiles_filename+App.data.user.foto.core_cloudfiles_extension;
            console.log(img);
            $('.profDatos .foto').html('<img src="'+img+'">');
        }
        else
        {
            if (App.data.user.tv_usuarios_avatar != '' && App.data.user.tv_usuarios_avatar != null)
            {
                if (App.data.user.tv_usuarios_avatar.indexOf('pbs.twimg.com')!=-1)
                {
                    App.data.user.tv_usuarios_avatar =App.data.user.tv_usuarios_avatar.replace("_normal", "_bigger");
                    //twitter
                    console.log('twitter foto');
                    $('.profDatos .foto').html('<img src="'+App.data.user.tv_usuarios_avatar+'">');
                }
                else
                {
                    //facebook
                    console.log('fb foto');
                    $('.profDatos .foto').html('<img src="'+App.data.user.tv_usuarios_avatar+'?type=large">');
                }
            }    
        }
        
        
        App.changePanel('#profile');
    }
    
    this.cambiarFoto = function()
    {
        $('.fotoSelect').fadeIn();
        
        //CAMERA
        App.pictureSource   = navigator.camera.PictureSourceType;
        App.destinationType = navigator.camera.DestinationType;
    }
    
    this.showCategoria = function(event)
    {
        var el      = $(event.delegateTarget);
        var param   = el.data('param');
        
        $('.hdTit').html(el.html());
        
        var botones = '';
        $.each(App.data.anuario,function(key,val)
        {
            if (val.elasegurador_anuario_giro_id == param)
            {
                $('.categoriaDesc').html(val.elasegurador_anuario_giro_descripcion);
                botones+= '<div class="catBtn" data-action="showDetalle" data-param="'+key+'"><div class="flecha"></div>'+val.elasegurador_anuario_nombre_empresa+'</div>';
            }
        });
        
        $('.catBtns').html(botones);
        
        App._attachActionEvents();
        App.changePanel('#categorias');
    }
    
    this.showDetalle = function(event)
    {
        var el          = $(event.delegateTarget);
        var param       = el.data('param');
        var next        = el.next();
        var compania    = App.data.anuario[param];
        
        if (next.attr('class')=='catAbierto')
        {
            el.find('.flecha').removeClass('selected');
            next.remove();
        }
        else
        {
            el.find('.flecha').addClass('selected');
            
            
            
            var html = '    <div class="catAbierto">'+
                                '<div class="catLogo">'+
                                    '<img src="'+App.rutaAbsImg+compania.foto[0].core_cloudfiles_filename+compania.foto[0].core_cloudfiles_extension+'">'+
                                '</div>'+
                                '<div class="catDesc">'+
                                    'Giro: '+compania.elasegurador_anuario_giro_nombre+'<br>'+
                                    '<br>'+
                                    compania.elasegurador_anuario_nombre_empresa+'<br>'+
                                    '<br>'+
                                    'Director General<br>'+
                                    compania.elasegurador_anuario_nombre_director_general+'<br>'+
                                    '<br>'+
                                    compania.elasegurador_anuario_descripcion+'<br>'+
                                    '<br>'+
                                    compania.elasegurador_anuario_direccion+'<br>'+                                    
                                    'Teléfono: '+compania.telefonos[0].elasegurador_anuario_telefono_numero+'<br>'+
                                    'Mail: '+compania.elasegurador_anuario_mail+'<br>'+
                                    'Web: <a href="#" onclick="window.open(\''+compania.elasegurador_anuario_web+'\', \'_blank\', \'location=yes\')">'+compania.elasegurador_anuario_web+'</a><br>'+
                                '</div>'+
                            '</div>';
            el.after(html);
        }
        
        
        
    }
    
    this.showPremium = function()
    {
        App.generateCanales();
        App.changePanel('#premium');
    }
    
    this.pagoPaypal = function()
    {
        App.popupMessage('Paypal','Funcionalidad para pago...');
    }
    
    this.editarGustos = function()
    {
        //console.log(App.data.user);
        
        App.data.generosUsuario = new Array();
        $.each(App.data.user.generos,function(i,el)
        {
            App.data.generosUsuario.push(el.tv_generos_id);
        });
        
        
        $('#generos .contadorGeneros').html(App.data.generosUsuario.length+'/5');
        $('#generos .generosTitulo').html('Editar tus gustos');
        
        
        $('#container').hide();
        
        //console.log(App.data.generosUsuario);
        
        var html = '';
        
        $.each(App.data.generos,function(i,val)
        {  
            var color = 'azul';
        
            //console.log(val.tv_generos_id+ ' '+App.data.generosUsuario.indexOf(val.tv_generos_id));
            if(App.data.generosUsuario.indexOf(val.tv_generos_id)!=-1)
            {
                color = 'verde';
            }
            
            html += '<div class="bubble '+color+'" data-action="selectGenero" data-id="'+val.tv_generos_id+'"><span>'+val.tv_generos_nombre+'</span></div>';//code
        });
        
        
        $('.bubbles').html(html);
        $('#container').fadeIn();
       
        
        setTimeout(function()
        {
            App.setLayoutBubbles();            
            App._attachActionEvents();
            
        },600)
    
        //$('.grid').css('height','auto');
        //$('.generos').jGravity();
        App.changePanel('#generos');
        
        $('.hdEdit').attr('data-action','editarGustosServer');
        $('.hdEdit').show();
        $('.hdFlecha.blanca').show();
    }
    
    this.editarGustosServer = function()
    {
        if (App.data.generosUsuario.length>0)
        {
            var data =
            {
                'idUser' : App.data.user.tv_usuarios_id,
                'generos': App.data.generosUsuario
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'editUserGeneros',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log('done...');
                console.log(data);
                
                window.localStorage['usrJSON'] = data;
            
                data = JSON.parse(data);
                App.data.user = data;
            });
            
            App.changePanel('#profile');
        }
        else
        {
            App.popupMessage('TV Discover','Favor de seleccionar al menos un género.');
        }
    }
    
    this.editarCanales = function()
    {
        App.data.canalesUsuario = new Array();
        
        $.each(App.data.user.canales,function(i,el)
        {
            App.data.canalesUsuario.push(el.tv_canales_id);
        });
        
        var html = '';
        $.each(App.data.canales.publicos,function(i,val)
        {
            if (App.data.user.tv_usuarios_region == val.tv_canales_region)
            {
                var clase = '';
                if(App.data.canalesUsuario.indexOf(val.tv_canales_id)!=-1)
                {
                    clase = 'selected';
                }
                
                var img = App.rutaAbsImg+val.foto.core_cloudfiles_filename+val.foto.core_cloudfiles_extension;
            
                html += '<div class="canal '+clase+'" data-action="selectCanal" data-id="'+val.tv_canales_id+'">'+
                //html += '<div class="canal">'+
                            '<div class="foto"><img src="'+img+'"></div>'+
                            '<div class="nombre">'+val.tv_canales_nombre+'</div>'+
                        '</div>';
            }
            
        });
        
        $.each(App.data.canales.privados,function(i,val)
        {
            var clase = '';
            if(App.data.canalesUsuario.indexOf(val.tv_canales_id)!=-1)
            {
                clase = 'selected';
            }
                
            var img = App.rutaAbsImg+val.foto.core_cloudfiles_filename+val.foto.core_cloudfiles_extension;
            
            html += '<div class="canal '+clase+'" data-action="selectCanal" data-id="'+val.tv_canales_id+'">'+
            //html += '<div class="canal">'+
                        '<div class="foto"><img src="'+img+'"></div>'+
                        '<div class="nombre">'+val.tv_canales_nombre+'</div>'+
                    '</div>';
        });
        
        $('.canales').html(html);
        
        $('.canalesTitulo').html('Editar tus canales');
        App.changePanel('#canales');
        
        $('.hdEdit').attr('data-action','editUserCanales');
        $('.hdEdit').show();
        $('.hdFlecha.blanca').show();
        
        App._attachActionEvents();
    }
    
    this.selectCanal = function(event)
    {
        var el      = $(event.delegateTarget);
        var id      = el.data('id');
        
        if (el.hasClass('selected'))
        {
            el.removeClass('selected');
            
            var indiceBorrar = -1;
            
            $.each(App.data.canalesUsuario,function(i,val)
            {
                if (val==id)
                {
                    indiceBorrar = i;
                    return -1;
                }
            });
            
            if (indiceBorrar > -1)
            {
                App.data.canalesUsuario.splice(indiceBorrar, 1);
            }
        }
        else
        {
            el.addClass('selected');
            App.data.canalesUsuario.push(id);
        }
        
        //$('.canalesReinicio').show();
        //App.data.canalesUsuario.push(id);
        
        
    }
    
    this.editUserCanales = function()
    {
        if (App.data.canalesUsuario.length>0)
        {
            var data =
            {
                'idUser' : App.data.user.tv_usuarios_id,
                'canales': App.data.canalesUsuario
            };
            
            var posting = $.post(App.intfc,
            {
                action  : 'editUserCanales',
                data    : data
            });
            
            posting.done(function(data)
            {
                console.log('done...');
                console.log(data);
                
                window.localStorage['usrJSON'] = data;
            
                data = JSON.parse(data);
                App.data.user = data;
            });
            
            App.changePanel('#profile');
        }
        else
        {
            App.popupMessage('TV Discover','Favor de seleccionar al menos un canal.');
        }
    }
    
    /*********************************************************
     *             GENERAL
     *********************************************************/
    
    this.randomIntFromInterval = function(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }
    
    /**
    * Registra los eventos onClick en todos aquellos nodos que tengan definido el atributo 'data-action'
    *
    * Además de registrar los eventos toma el valor del atributo para generar la función que se ejecutará en el evento.
    */
    this._attachActionEvents = function()
    {
        $('*[data-action]').unbind( "click" );

        $('*[data-action]').click(function(event)
        {
            var data 	= $(this).attr( "data-action" );
            window['App'][data](event);
        });
    }; // EOF

    /*
     * Detecta mobile device
     */
    this.isMobile = {
	Android: function() {
	    return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
	    return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
	    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
        iPad: function() {
	    return navigator.userAgent.match(/iPad/i);
	},
	Opera: function() {
	    return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
	    return navigator.userAgent.match(/IEMobile/i);
	},
	any: function() {
	    return (Site.isMobile.Android() || Site.isMobile.BlackBerry() || Site.isMobile.iOS() || Site.isMobile.Opera() || Site.isMobile.Windows());
	}
    }
    
    /*********************************************************
     * CAMERA
     *********************************************************/
    this.capturePhoto = function(){
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(App.onPhotoURISuccess, App.onFail,
        {
            quality: 80,
            destinationType: App.destinationType.FILE_URI,
            encodingType: Camera.EncodingType.JPEG
        });
    }
    
    this.getPhotoLibrary = function()
    {
        source = App.pictureSource.PHOTOLIBRARY;
        
        // Retrieve image file location from specified source
        navigator.camera.getPicture(App.onPhotoURISuccess, App.onFail,
        {
            quality: 80,
            destinationType: App.destinationType.FILE_URI,
            sourceType: source,
            encodingType: Camera.EncodingType.JPEG
        });
    }

    // Called when a photo is successfully retrieved    
    this.onPhotoURISuccess = function(imageURI)
    {
        $('.fotoSelect').hide();
        App.popupMessage('Registro','Cargando foto...');

        $('.profDatos .foto').html('<img src="'+ imageURI+'">');
        
        console.log(imageURI);
        
        

        //$('.regUserFoto').css('background','none');
        //MOVER A ARCHIVO LOCAL
        //App.movePic(imageURI);
        
        
        //SUBE A SERVIDOR
        //Instalar cordova plugin add org.apache.cordova.file-transfer
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;

        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI(App.intfc+'?action=uploadFoto&idUser='+App.data.user.tv_usuarios_id), App.win, App.onFail, options);
        
        var posting = $.post(App.post,
        {
            json	: 'user',
            idUser  : App.data.user.tv_usuarios_id
        });

        posting.done(function(data)
        {
            window.localStorage['usrJSON'] = data;
            
            data = JSON.parse(data);
            App.data.user = data;
            
            console.log(App.data.user);
            
            //var img = App.rutaAbsImg+App.data.user.foto.core_cloudfiles_filename+App.data.user.foto.core_cloudfiles_extension;            
            //$('.profDatos .foto').html('<img src="'+img+'">');
            
        });
    }
    
    

    this.movePic = function(file)
    {
        window.resolveLocalFileSystemURI(file, App.saveFileLocal, App.onFail); 
    }
    
    this.saveFileLocal = function(entry)
    {
        //console.log('*********** save file');
        
        var d = new Date();
        var n = d.getTime();
        //new file name
        var newFileName = n + ".jpg";
        var myFolderApp = "TVDiscover";
    
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys)
        {      
            //The folder is created if doesn't exist
            fileSys.root.getDirectory
            (
                myFolderApp,
                {
                    create:true,
                    exclusive: false
                },
                function(directory)
                {
                    entry.moveTo(directory, newFileName,  App.successMove, App.onFail);
                },
                App.onFail
            );
        },
        App.onFail);
    }
    
    //Callback function when the file has been moved successfully - inserting the complete path
    this.successMove = function(entry)
    {
        //console.log('successMovesuccessMovesuccessMovesuccessMovesuccessMove');
        //I do my insert with "entry.fullPath" as for the path
        
        App.rutaImagen = entry.fullPath;
        //App.rutaImagen = entry.toURL();
        console.log(App.rutaImagen);
        
        $('.profDatos .foto').html('<img src="'+App.rutaImagen+'">');
        
        App.getImageLocal(App.rutaImagen);
        
    }
    
    
    
    this.onFail = function(message)
    {
        console.log('Failed because: ' + message);
        console.log(message.code);
        App.popupMessage('Registro','Se presento un error al tratar de obtener la foto.');
    }
    
    this.getImageLocal = function(archivo)
    {
        //archivo = '/DMA/1437601492432.jpg';
        var image = '';
        
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
        function(fileSystem)
        {
            fileSystem.root.getFile(archivo, null,
            function(fileEntry)
            {
                fileEntry.file(
                    function(file)
                    {
                        var reader = new FileReader();
                        reader.onloadend = function(evt)
                        {
                            //console.log("Read as data URL");
                            //console.log(evt.target.result);
                            
                            $('.profDatos .foto').html('<img src="'+evt.target.result+'">');
                            //$('.menuFoto img').attr('src',evt.target.result);
                            //$('.perfilFoto img').attr('src',evt.target.result);
                            
                            return image;
                        };
                        
                        reader.readAsDataURL(file);
                    },
                    function()
                    {
                        console.log('error 1');
                    });
            },
            function(error)
            {
                console.dir(error);
            });
        },
        function()
        {
            console.log('error 3');
        });
    }
    
    /***********************************************
     *     JSON
     **********************************************/
    this.checkJSON = function()
    {
        //console.log(window.localStorage['usrJSON']);
        if (window.localStorage['usrJSON'] != '' && window.localStorage['usrJSON'] != undefined)
        {
            App.data.user = JSON.parse(window.localStorage['usrJSON']);
        }
        
        var posting = $.post(App.post,
        {
            json	: 'canales-size',
        });
        
        posting.done(function(data)
        {
            if (window.localStorage['canSize']=='' || window.localStorage['canSize'] == undefined)
            {
                console.log('CANALES no definido, actualizar');
                window.localStorage['canSize'] = data;
                
                App.getJSON();
            }
            else
            {
                if (data != window.localStorage['canSize'])
                {
                    console.log('CANALES en server distinto , actualizar');
                    window.localStorage['canSize'] = data;
                    App.getJSON();
                }
                else
                {
                    if (window.localStorage['canJSON']=='' || window.localStorage['canJSON'] == undefined)
                    {
                        console.log('canJSON no definido, actualizar');
                        App.getJSON();
                    }
                    else
                    {
                        console.log('canJSON definido, NO actualizar');
                        
                        var posting = $.post(App.post,
                        {
                            json	: 'generos-size',
                        });
                        
                        posting.done(function(data)
                        {
                            if (window.localStorage['genSize']=='' || window.localStorage['genSize'] == undefined)
                            {
                                console.log('GENEROS no definido, actualizar');
                                window.localStorage['genSize'] = data;
                                
                                App.getJSON();
                            }
                            else
                            {
                                if (data != window.localStorage['genSize'])
                                {
                                    console.log('GENEROS en server distinto , actualizar');
                                    App.getJSON();
                                }
                                else
                                {
                                    if (window.localStorage['genJSON']=='' || window.localStorage['genJSON'] == undefined)
                                    {
                                        console.log('genJSON no definido, actualizar');
                                        App.getJSON();
                                    }
                                    else
                                    {
                                        console.log('genJSON definido, NO actualizar');
                                        App.init();
                                    }
                                }
                            }
                        });
                        
                    }
                }
            }
            
            
        });
        
    }
    
    this.getJSON = function()
    {
        var posting = $.post(App.post,
        {
            json	: 'canales',
        });

        posting.done(function(data)
        {
            window.localStorage['canJSON'] = data;
            
            data = JSON.parse(data);
            App.data.canales = data;
            
            //console.log(App.data.canales);
           var posting = $.post(App.post,
            {
                json	: 'generos',
            });
    
            posting.done(function(data)
            {
                window.localStorage['genJSON'] = data;
                
                data = JSON.parse(data);
                App.data.generos = data;
                
                //console.log(App.data.canales);
                App.init();
            });
        });
    }
    
    this.updateJsons = function()
    {
        $('#splashscreen').html('<center><img src="assets/images/logo.png"></center><br />');
        $('#splashscreen').append('<span style="color:#000;">Cargando datos...</span><br />');

        var posting = $.post(App.post,
        {
            json	: 'anuario',
        });

        posting.done(function(data)
        {
            data = JSON.parse(data);
            window.localStorage['anuJSON'] = data;
            App.data.anuario = data;

            $.afui.drawer.hide();

            App.init();
        });
    }
    
    this.reintentarLaunch = function()
    {
        var conexion = checkConnection();

        if (conexion == 'No network connection')
        {
            $('#splashscreen').html('<div class="splashAlign">'+
                                    '<img src="assets/images/logo.png" class="splashLogo"><br />'+
                                    '<span style="color:#000;">No tienes conexión de internet.</span><br /><br />'+
                                    '<span style="color:#000;" data-action="reintentarLaunch">Probar de nuevo haciendo click aquí.</a>'+
                                '</div>');

            App._attachActionEvents();
        }
        else
        {
             $('#splashscreen').html('<img src="assets/images/loading_3.gif">');
            App.updateJsons();
        }

    }
    
};

window.onerror = function(msg, file, line)
{
    alert(msg + '; ' + file + '; ' + line);
};

var success = function(message) { alert("Success: " + JSON.stringify(message)); };
var error = function(message) { alert("Error: " + message); };